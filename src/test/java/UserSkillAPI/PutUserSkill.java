package UserSkillAPI;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.json.simple.JSONObject;

import LMSXlUtility.Xutility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PutUserSkill {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/UserSkills/";
	}
	RequestSpecification putuserskillrequest;
	Response userskillresponse;
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/UserSkill.xlsx";
	
	@SuppressWarnings("unchecked")
	@Given("User updates userid,skillid,mthsexperiences in request body using end point Url\\/Users\\/userskillid")
	public void user_updates_userid_skillid_mthsexperiences_in_request_body_using_end_point_url_users_userskillid() throws IOException {
		String userid = Xutility.getCellData(path, "PutUserSkill", 1, 1);
		String skillid = Xutility.getCellData(path, "PutUserSkill", 1, 2);
		String monthsofExperiences = Xutility.getCellData(path, "PutUserSkill", 1, 3);
		
		JSONObject requestput = new JSONObject();
		requestput.put("user_id", userid);
		requestput.put("skill_id", skillid);
		requestput.put("months_of_exp", monthsofExperiences);
		putuserskillrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User clicks PUT request for userskillid")
	public void user_clicks_put_request_for_userskillid() throws IOException {
		String userskillid = Xutility.getCellData(path, "PutUserSkill", 1, 0);
		userskillresponse = putuserskillrequest.when().put(userskillid);
	}

	@Then("User should see the userid,skillid, mthsexperiencess with userskillid in response body with status code {string}")
	public void user_should_see_the_userid_skillid_mthsexperiencess_with_userskillid_in_response_body_with_status_code(String string) throws IOException {
		userskillresponse.then().log().all();
		System.out.println("UserSkill Details are Updated");
		int StatusCode = userskillresponse.getStatusCode();
		String userskillrespvalidation = userskillresponse.asPrettyString();
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		System.out.println("=========================");
		assertEquals(StatusCode, 201);
		assertEquals(true, userskillrespvalidation.contains("user_skill_id"));
		assertEquals(true, userskillrespvalidation.contains("user_id"));
		assertEquals(true, userskillrespvalidation.contains("skill_id"));
		System.out.println("Response " + StatusCode + " Successfull");
		System.out.println("Schema Validation");
		System.out.println("==================");
		userskillresponse.then().assertThat().body(
				JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUserSkill.Json")))
				.log().all();
		System.out.println("Successfully validated schema for a user");
		System.out.println("================================================");
		System.out.println("DB Validation");
		System.out.println("==============");
		JsonPath userskillres = new JsonPath(userskillrespvalidation);
		String userid = userskillres.getString("user_id");
		String skillid = userskillres.getString("skill_id");
		String userskillid=userskillres.getString("user_skill_id");
		System.out.println("userskillid:" +userskillid + ",userid :" +userid +",skillid:"+skillid);
		String userids = Xutility.getCellData(path, "PutUserSkill", 1, 1);
		String skillids=Xutility.getCellData(path, "PutUserSkill", 1, 2);
		String userskillids = Xutility.getCellData(path, "PutUserSkill", 1, 0);
		System.out.println("userskillids:" +userskillids+ ",userids :" +userids+",skillids :"+skillids);
		assertEquals(userskillid, userskillids);
		assertEquals(userid, userids);
		assertEquals(skillid, skillids);
		System.out.println("***************************************");
	}

	@SuppressWarnings("unchecked")
	@Given("User update userid,skillid,mthsexperiences in request body using end point Url\\/Users\\/userskillid")
	public void user_update_userid_skillid_mthsexperiences_in_request_body_using_end_point_url_users_userskillid() throws IOException {
		String userid = Xutility.getCellData(path, "PutUserSkill", 2, 1);
		String skillid = Xutility.getCellData(path, "PutUserSkill", 2, 2);
		String monthsofExperiences = Xutility.getCellData(path, "PutUserSkill", 2, 3);
		
		JSONObject requestput = new JSONObject();
		requestput.put("user_id", userid);
		requestput.put("skill_id", skillid);
		requestput.put("months_of_exp", monthsofExperiences);
		putuserskillrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User click PUT request for userskillid")
	public void user_click_put_request_for_userskillid() throws IOException {
		String userskillid = Xutility.getCellData(path, "PutUserSkill", 2, 0);
		userskillresponse = putuserskillrequest.when().put(userskillid);
	}

	@Then("User should see UserSkill id not Found in response body with status code {string}")
	public void user_should_see_user_skill_id_not_found_in_response_body_with_status_code(String string) {
		userskillresponse.then().log().all();
		int StatusCode = userskillresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("UserSkill Id is not found");
		
		
		
		
		
		
		
		
		
	}

}
