package UserSkillAPI;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.io.File;

import org.junit.Assert;

public class GetUserSkillRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com/";
		RestAssured.basePath = "UserSkills";
	}
	RequestSpecification requestget;
	Response userskillresponse;

	@Given("User enters the endpoint url\\/UserSkill with invalid authorization")
	public void user_enters_the_endpoint_url_user_skill_with_invalid_authorization() {
		requestget = given().contentType("application/json").auth().basic("PROCESSING", "2xxSuccess");
	}
	
	
	@When("User clicks on get request for UserSkill")
	public void user_clicks_on_get_request_for_user_skill() {
		userskillresponse = requestget.when().get();
	}

	@Then("User should see Unauthorized error with status code\"{int}\"")
	public void user_should_see_unauthorized_error_with_status_code(Integer int1) {
		userskillresponse.then().log().all();
		int statuscode = userskillresponse.getStatusCode();
		Assert.assertEquals(statuscode, 401);
		System.out.println("============================================");
		System.out.println("Status Code :" + statuscode);
		System.out.println("Response " + statuscode + " Successfull");
		System.out.println("============================================");
	}

	
	@Given("User enters the endpoint url\\/UserSkill with valid authorization")
	public void user_enters_the_endpoint_url_user_skill_with_valid_authorization() {
		requestget = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");
	}

	@When("User clicks get request for UserSkill")
	public void user_clicks_get_request_for_user_skill() {
		userskillresponse = requestget.when().get();
	}

	@Then("User should see all user_skill_id with user_id,skill_id,months_of_exp with status code\"{int}\"")
	public void user_should_see_all_user_skill_id_with_user_id_skill_id_months_of_exp_with_status_code(Integer int1) {
		userskillresponse.then().log().all();
		userskillresponse.getBody();

		int statuscode = userskillresponse.getStatusCode();
		String statusLine = userskillresponse.getStatusLine();

		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 ");

		System.out.println("============================================");
		System.out.println("Response " + statuscode + " Successfull");
		System.out.println(statusLine);

		userskillresponse.then().assertThat()
				.body(JsonSchemaValidator
						.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUserSkills.Json")))
				.log().all();
		userskillresponse.then().log().all();

		System.out.println("Successfully validated schema for all the users");
		System.out.println("================================================");

	}

	@Given("User enters the endpoint url\\/UserSkill\\/id for particular UserSkillid")
	public void user_enters_the_endpoint_url_user_skill_id_for_particular_user_skillid() {
		requestget = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");
	}

	@When("User clicks get request for particular {string}")
	public void user_clicks_get_request_for_particular(String string) {
		String user_skill_id = string;
		userskillresponse = requestget.when().get(user_skill_id);
	}

	@Then("User should see user_skill_id with user_id,skill_id,months_of_exp for UserSkillid with statuscode\"{int}\"")
	public void user_should_see_user_skill_id_with_user_id_skill_id_months_of_exp_for_user_skillid_with_statuscode(
			Integer int1) {
		userskillresponse.then().log().all();
		int statuscode = userskillresponse.getStatusCode();
		if (statuscode == 200) {
			System.out.println("****************************************");
			System.out.println("Response Body Validation");
			System.out.println("==========================");
			String Resvalidation = userskillresponse.asPrettyString();
			assertEquals(statuscode, 200);
			assertEquals( true,Resvalidation.contains("skill_id"));
			assertEquals( true,Resvalidation.contains("months_of_exp"));
			assertEquals( true,Resvalidation.contains("user_id"));
			assertEquals( true,Resvalidation.contains("user_skill_id"));
			
			System.out.println("Response " + statuscode + " Successfull");
			System.out.println("Schema Validation");
			System.out.println("===================");
			userskillresponse.then().assertThat()
					.body(JsonSchemaValidator
							.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUserSkill.Json")))
					.log().all();
			System.out.println("Successfully validated schema for a particular userskill");
			System.out.println("============================================");
			} 
		    else if (statuscode == 404) {
			System.out.println("*******************************************");	
			System.out.println("Response Body Validation");
			System.out.println("==========================");
			System.out.println("Response " + statuscode + " Successfull");
			userskillresponse.then().log().all(); 
			System.out.println("*******************************************");
			
		}

	}

}
