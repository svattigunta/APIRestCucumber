package LMSXlUtility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xutility {
	public static FileInputStream fi;
	public static FileOutputStream fo;
	public static XSSFWorkbook workbook;
	public static XSSFSheet sheet;
	public static XSSFRow row;
	public static XSSFCell cell;
	public CellStyle style;
	
	public static int getRowCount(String xlFile,String sheetName) throws IOException {
		fi = new FileInputStream(xlFile);
		workbook = new XSSFWorkbook(fi);
		sheet = workbook.getSheet(sheetName);
		int rowcount = sheet.getLastRowNum();
		workbook.close();
		fi.close();
		return rowcount;
	}

	public static int getCellCount(String xlFile,String sheetName, int rownum) throws IOException {
		fi = new FileInputStream(xlFile);
		workbook = new XSSFWorkbook(fi);
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rownum);
		int cellcount = row.getLastCellNum();
		workbook.close();
		fi.close();
		return cellcount;

	}
	
	public static String getCellValue(String xlFile,String sheetName,int rownum) throws IOException
    {
      fi=new FileInputStream(xlFile);
      workbook=new XSSFWorkbook(fi);
      sheet=workbook.getSheet(sheetName);
      row=sheet.getRow(rownum);
      DataFormatter formatter = new DataFormatter();
      String data;
      try {
      data=formatter.formatCellValue(cell);
      }
      catch(Exception e)
      {
      data="";
      }

     workbook.close();
     fi.close();
     return data ;
     }

    public static String getCellData(String xlFile,String sheetName,int rownum, int colnum) throws IOException
     {
       fi=new FileInputStream(xlFile);
       workbook=new XSSFWorkbook(fi);
       sheet=workbook.getSheet(sheetName);
       row=sheet.getRow(rownum);
       cell=row.getCell(colnum);

       DataFormatter formatter = new DataFormatter();
       String data;
       try {
       data=formatter.formatCellValue(cell);
       }
       catch(Exception e)
       {
       data="";
       }

      workbook.close();
      fi.close();
      return data ;
      }
    public static void setCellData(String xlFile, String sheetName,int rownum, int colnum, String data) throws IOException {
    	fi = new FileInputStream(xlFile);
    	workbook = new XSSFWorkbook(fi);
    	sheet = workbook.getSheet(sheetName);
    	row = sheet.getRow(rownum);
    	cell = row.createCell(colnum);
    	cell.setCellValue(data);
    	fo = new FileOutputStream(xlFile);
    	workbook.write(fo);
    	workbook.close();
    	fo.close();
    	fi.close();
    	}
    
    
	}

