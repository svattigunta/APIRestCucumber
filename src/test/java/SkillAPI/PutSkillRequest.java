package SkillAPI;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.json.simple.JSONObject;

import LMSXlUtility.Xutility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PutSkillRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/Skills";
	}
	RequestSpecification putskillrequest;
	Response skillresponse;
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/SKILLSAPI.xlsx";

	@SuppressWarnings("unchecked")
	@Given("User updates skill name in request body using the end point Url\\/Skills\\/skillid")
	public void user_updates_skill_name_in_request_body_using_the_end_point_url_skills_skillid() throws IOException {

		String skillname = Xutility.getCellData(path, "PutSkill", 1, 1);
		JSONObject requestput = new JSONObject();
		// requestput.put("skill_id",skillid);
		requestput.put("skill_name", skillname);
		putskillrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User clicks PUT request")
	public void user_clicks_put_request() throws IOException {
		String skillid = Xutility.getCellData(path, "PutSkill", 1, 0);
		skillresponse = putskillrequest.when().put(skillid);
	}

	@Then("User should see the skill_id,name in response body with status code {string}")
	public void user_should_see_the_skill_id_name_in_response_body_with_status_code(String string) throws IOException {
		skillresponse.then().log().all();
		System.out.println("Skill Updated");
		String Resvalidation = skillresponse.asPrettyString();
		int StatusCode = skillresponse.getStatusCode();
		
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		System.out.println("=========================");
		assertEquals(StatusCode, 201);
		assertEquals(true, Resvalidation.contains("skill_id"));
		assertEquals(true, Resvalidation.contains("skill_name"));
		System.out.println("Response " + StatusCode + " Successfull");
		System.out.println("Schema Validation");
		System.out.println("==================");
		skillresponse.then().assertThat().body(
				JsonSchemaValidator.matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetSkill.Json")))
				.log().all();
		System.out.println("Successfully validated schema for a skill");
		System.out.println("================================================");
		System.out.println("DB Validation");
		System.out.println("==============");
		JsonPath reval = new JsonPath(Resvalidation);
		String skillid = reval.getString("skill_id");
		String skillname = reval.getString("skill_name");
		System.out.println("Skill id:" + skillid + ",Skill name :" + skillname);
		String id = Xutility.getCellData(path, "PutSkill", 1, 0);
		String name = Xutility.getCellData(path, "PutSkill", 1, 1);
		System.out.println("Skill id:" + id + ",Skill name :" + name);
		assertEquals(skillid,id);
		assertEquals(skillname,name);
		System.out.println("***************************************");
	}

	@SuppressWarnings("unchecked")
	@Given("User update skill name in request body using end point Url\\/Skills\\/skillid")
	public void user_update_skill_name_in_request_body_using_end_point_url_skills_skillid() throws IOException {
		String skillname = Xutility.getCellData(path, "PutSkill", 2, 1);
		JSONObject requestput = new JSONObject();
		requestput.put("skill_name", skillname);
		putskillrequest = given().auth().basic("APIPROCESSING", "2xx@Success").contentType("application/json")
				.body(requestput.toJSONString());
	}

	@When("User click Put request")
	public void user_click_put_request() throws IOException {
		String skillid = Xutility.getCellData(path, "PutSkill", 2, 0);
		skillresponse = putskillrequest.when().put(skillid);
	}

	@Then("User should see Skillid not Found in response body with status code {string}")
	public void user_should_see_skillid_not_found_in_response_body_with_status_code(String string) {
		skillresponse.then().log().all();
		int StatusCode = skillresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("Skill Not Found");
	}

}
