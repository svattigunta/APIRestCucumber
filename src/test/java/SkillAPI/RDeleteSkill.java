package SkillAPI;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import LMSXlUtility.Xutility;

public class RDeleteSkill {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/Skills/";
	}
	RequestSpecification Deleteskillrequest;
	Response skillresponse;
	
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/SKILLSAPI.xlsx";
	
	
	@Given("User enters valid skillid in the endpoint URL\\/Skills")
	public void user_enters_valid_skillid_in_the_endpoint_url_skills() {
		Deleteskillrequest =given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User clicks on Delete request with skill id")
	public void user_clicks_on_delete_request_with_skill_id() throws IOException {
		String skillid=Xutility.getCellData(path,"DeleteSkill", 1, 0);
		skillresponse=Deleteskillrequest.when().delete(skillid);
	}
	
	@Then("User should see skill id deleted message in Response Body with status code {string} and skill id should not be seen in console")
	public void user_should_see_skill_id_deleted_message_in_response_body_with_status_code_and_skill_id_should_not_be_seen_in_console(String string) {
		skillresponse.then().log().all();
		System.out.println("Skill id is Deleted");
		int StatusCode =skillresponse.getStatusCode();
		
		String Resvalidation = skillresponse.asPrettyString();
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		System.out.println("=========================");
		assertEquals(StatusCode, 200);
		System.out.println("Response " + StatusCode + " Successfull");
		System.out.println("DB Validation");
		System.out.println("==============");
		JsonPath reval = new JsonPath(Resvalidation);
		String skillid = reval.getString("skill_id");
		String skillname = reval.getString("skill_name");
		System.out.println("Skill id:" + skillid + ",Skill name :" + skillname);
		assertEquals(skillid,null);
		assertEquals(skillname,null);
		System.out.println("***************************************");
		
	}

	@Given("User enters invalid skillid in the endpoint URL\\/Skills")
	public void user_enters_invalid_skillid_in_the_endpoint_url_skills() {
		Deleteskillrequest =given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User click on Delete request with skill id")
	public void user_click_on_delete_request_with_skill_id() throws IOException {
		String skillid=Xutility.getCellData(path,"DeleteSkill", 2, 0);
		skillresponse=Deleteskillrequest.when().delete(skillid);
	}

	@Then("User should see skill id not found message in Response Body with status code {string}")
	public void user_should_see_skill_id_not_found_message_in_response_body_with_status_code(String string) {
		skillresponse.then().log().all();
		int StatusCode =skillresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("Skill id not Found");
	}

}
