package UsersAPI;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
//import io.restassured.RestAssured.*;

import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.io.File;

import org.junit.Assert;

public class GetUsersRequest {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com/";
		RestAssured.basePath = "Users/";

	}
	RequestSpecification get_request;
	Response userresponse;
	
	@Given("User enters Url in the endpint url\\/Users")
	public void user_enters_url_in_the_endpint_url_users() {
		get_request = given().contentType("application/json").auth().basic("APX", "Processor");
	}
	@When("Users then clicks the request")
	public void users_then_clicks_the_request() {
		userresponse = get_request.when().get();
	}
	@Then("User request should see the Unauthorized error")
	public void user_request_should_see_the_unauthorized_error() {
		userresponse.then().log().all();
		int statuscode = userresponse.getStatusCode();
		Assert.assertEquals(statuscode, 401);
		System.out.println("============================================");
		System.out.println("Status Code :" + statuscode);
		System.out.println("Reponse " + statuscode + " Successfull");
		System.out.println("============================================");
	}

	@Given("User enter Url the endpint url\\/Users")
	public void user_enter_url_the_endpint_url_users() {
		get_request = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");
	}

	@When("Users then clicks the Get request")
	public void users_then_clicks_the_get_request() {
		userresponse = get_request.when().get();
	}
	
	@Then("User should see the user_id,name and other details will be the response in the JSON format with Status code:{string}")
	public void user_should_see_the_user_id_name_and_other_details_will_be_the_response_in_the_json_format_with_status_code(String string) {
		
		userresponse.getBody();

		int statuscode = userresponse.getStatusCode();
		String statusLine = userresponse.getStatusLine();

		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(statusLine, "HTTP/1.1 200 ");

		System.out.println("============================================");
		System.out.println("Response " + statuscode + " Successfull");
		System.out.println(statusLine);
		//userresponse.then().log().all();
		
		  userresponse.then().assertThat().body(
		  JsonSchemaValidator.matchesJsonSchema(new
		  File("src/test/resources/SchemaValidation/GetUsers.Json"))) .log().all();
		 
		System.out.println("Successfully validated schema for all the users");
		System.out.println("================================================");
	}
	
	@Given("User is in request Get by Using the endpoint")
	public void user_is_in_request_get_by_using_the_endpoint() {
		get_request = given().contentType("application/json").auth().basic("APIPROCESSING", "2xx@Success");
	}
	
	@When("User clicks the request GET  {string}")
	public void user_clicks_the_request_get(String string) {
		String user_id = string;
		userresponse = get_request.when().get(user_id);
	}
	
	@Then("User should see the Specified user_id with user details will be the response in JSON format with Status code:{string}")
	public void user_should_see_the_specified_user_id_with_user_details_will_be_the_response_in_json_format_with_status_code(String string) {
		
		int statuscode = userresponse.getStatusCode();
		if (statuscode == 200) {
		  userresponse.then().log().all();	
		  System.out.println("****************************************");
		  System.out.println("Response Body Validation");
		  System.out.println("==========================");
		  String Resvalidation = userresponse.asPrettyString();
			assertEquals( true,Resvalidation.contains("user_id"));
			assertEquals( true,Resvalidation.contains("name"));
		  assertEquals(statuscode, 200);
		  System.out.println("Response " + statuscode + " Successfull");
		  System.out.println("Schema Validation");
		  System.out.println("===================");
		  userresponse.then().assertThat().body(JsonSchemaValidator
		  .matchesJsonSchema(new File("src/test/resources/SchemaValidation/GetUser.Json"))) .log().all();
		  System.out.println("Successfully validated schema for a particular user"); 
		  System.out.println("**********************************************"); 
		  } 
		else if (statuscode == 404) {
		  System.out.println("**********************************************"); 
		  System.out.println("Response Body Validation");	
		  System.out.println("===========================");
		  System.out.println("Response " + statuscode + " Successfull");
		  userresponse.then().log().all();
		  System.out.println("*******************************************");
			}
		 
	}
	
}
	

	
	
	