package UsersAPI;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import LMSXlUtility.Xutility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RDeleteUser {
	{
		RestAssured.baseURI = "https://springboot-lms-userskill.herokuapp.com";
		RestAssured.basePath = "/Users/";
	}
	RequestSpecification Deleteuserrequest;
	Response userresponse;
	
	String path = "C:/Sunandha/Eclipse/RestAssuredAPICucumber/src/test/resources/Xceldata/USERSAPI.xlsx";
	
	@Given("User enters valid userid in the endpoint URL\\/Users")
	public void user_enters_valid_userid_in_the_endpoint_url_users() {
		Deleteuserrequest =given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User clicks on Delete request with userid")
	public void user_clicks_on_delete_request_with_userid() throws IOException {
		String UserId=Xutility.getCellData(path,"DeleteUser", 1, 0);
		userresponse=Deleteuserrequest.when().delete(UserId);
	}
	
	@Then("User should see userid deleted message in Response Body with status code {string} and userid should not be seen in console")
	public void user_should_see_userid_deleted_message_in_response_body_with_status_code_and_userid_should_not_be_seen_in_console(String string) {
		userresponse.then().log().all();
		System.out.println("User id is Deleted");
		String Resvalidation = userresponse.asPrettyString();
		int StatusCode =userresponse.getStatusCode();
		System.out.println("***************************************");
		System.out.println("Response Body Validation");
		System.out.println("=========================");
		assertEquals(StatusCode, 200);
		System.out.println("Response " + StatusCode + " Successfull");
		System.out.println("DB Validation");
		System.out.println("==============");
		JsonPath reval = new JsonPath(Resvalidation);
		String userid = reval.getString("user_id");
		String username = reval.getString("name");
		System.out.println("User id:" + userid + ",User name :" + username);
		assertEquals(userid,null);
		assertEquals(username,null);
		System.out.println("***************************************");
	}
	
	@Given("User enters invalid userid in the endpoint URL\\/Users")
	public void user_enters_invalid_userid_in_the_endpoint_url_users() {
		Deleteuserrequest =given().auth().basic("APIPROCESSING","2xx@Success");
	}

	@When("User click on Delete request with userid")
	public void user_click_on_delete_request_with_userid() throws IOException {
		String UserId=Xutility.getCellData(path,"DeleteUser", 2, 0);
		userresponse=Deleteuserrequest.when().delete(UserId);
	}

	@Then("User should see userid not found message in Response Body with status code {string}")
	public void user_should_see_userid_not_found_message_in_response_body_with_status_code(String string) {
		userresponse.then().log().all();
		int StatusCode =userresponse.getStatusCode();
		assertEquals(StatusCode, 404);
		System.out.println("User id not Found");
	}

}
