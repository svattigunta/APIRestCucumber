@DeleteUserSkill
Feature: Validate the Delete Request for UserSkill with Valid authorization

  @test01
  Scenario Outline: Delete the User with valid userskillid
    Given User enters valid userskillid in the endpoint URL/UserSkills
     When User clicks on Delete request with userskillid
     Then User should see userskillid deleted message in Response Body with status code "200" and userskillid should not be seen in console 


  @tag2
  Scenario Outline: Delete the User with invalid userskillid
    Given User enters invalid userskillid in the endpoint URL/UserSkills
    When  User click on Delete request with userskillid
    Then  User should see userskillid not found message in Response Body with status code "404"

    
