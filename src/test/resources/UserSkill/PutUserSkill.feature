
@PutUserSkill
Feature: Validate the PUT request for UserSkill with vaid authorization

  @test01
 Scenario Outline: User updates the details for valid userskill_id
    Given User updates userid,skillid,mthsexperiences in request body using end point Url/Users/userskillid
     When User clicks PUT request for userskillid
     Then User should see the userid,skillid, mthsexperiencess with userskillid in response body with status code "201"
     
  @test02
  Scenario Outline: User Updates the details for invalid userskill_id
    Given User update userid,skillid,mthsexperiences in request body using end point Url/Users/userskillid
    When  User click PUT request for userskillid
    Then  User should see UserSkill id not Found in response body with status code "404"

    