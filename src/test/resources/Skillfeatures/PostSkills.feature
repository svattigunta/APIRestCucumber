
@PostSkills
Feature: Validate Post request for Skills with valid authorization 
 

  @test01
  Scenario Outline: User trying to create a skill with new skill name
    Given User creates new skills and enters the end point Url/Skills
    When  User enters skill name in request body from sheet and click post request 
    Then  User should see autogenerated Skill_ids with Skill details in response body and Status code "201"  
   
  @test02
  Scenario Outline: User trying to create a skill with existing skill name
    Given User uses existing skill and enters the end point Url/Skills
    When  User enters <Skill_name> in request boby as input and clicks Post request
    Then  User should see Skill already exist with Status code "401"

   