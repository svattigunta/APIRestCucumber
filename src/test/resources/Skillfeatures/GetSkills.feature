
@GetSkill
Feature: Validate the Get request
 

  @test01
  Scenario: Validate the Get request without authorization
    Given User enters the endpint url/skills
    When Users clicks the request
    Then User should see the Unauthorized error
    
    
   @test02
  Scenario: Validate the GET request and display all the skills
    Given User enter in the endpoint url/skills
    When User clicks the Get request
    Then User should see the Skill_id,Skill_name will be the response in the JSON format with Status code:"200 OK"
   

  @test03
  Scenario Outline: Validate the GET request by giving specified id
    Given User is in Get request by Using the endpoint
    When User clicks the GET request "<skill_id>"
    Then User should see the Specified Skill_id with Skill_name will be the response in JSON format with Status code:"200 OK"

    Examples: 
      | skill_id | status  | 
      | 513      |success |
      | 100001     | Fail    |
    
     
      
      
  