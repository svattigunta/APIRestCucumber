
@GetUser
Feature: Title of your feature
  I want to use this template for my feature file

  @test01
  Scenario: Validate the Get Users request without authorization
    Given User enters Url in the endpint url/Users
    When  Users then clicks the request
    Then  User request should see the Unauthorized error
  

  @test02
  Scenario: Validate the Get Users request with authorization
    Given User enter Url the endpint url/Users
    When  Users then clicks the Get request
    Then  User should see the user_id,name and other details will be the response in the JSON format with Status code:"200 OK"

  
  @test03
  Scenario Outline: Validate the Get User request by giving Userid
    Given User is in request Get by Using the endpoint
    When  User clicks the request GET  "<user_id>"
    Then  User should see the Specified user_id with user details will be the response in JSON format with Status code:"200 OK"

    Examples: 
      | user_id | status  |
      | U2982    | success |
      | u373    | Fail    |
      
    
 