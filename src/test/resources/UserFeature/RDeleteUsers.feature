
@DeleteUser
Feature: Validate the Delete Request for User with Valid authorization

  @test01
    Scenario Outline: Delete the User with valid user_id 
    Given User enters valid userid in the endpoint URL/Users
    When  User clicks on Delete request with userid
    Then  User should see userid deleted message in Response Body with status code "200" and userid should not be seen in console

  @test02
  Scenario Outline: Delete the skill with invalid user_id
    Given User enters invalid userid in the endpoint URL/Users
    When  User click on Delete request with userid
    Then  User should see userid not found message in Response Body with status code "404"

   
