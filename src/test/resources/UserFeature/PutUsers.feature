  
@PutUser
Feature: Validate the PUT request for User with vaid authorization

  @test01
  Scenario Outline: User updates the details for valid user_id
    Given User updates user details in request body using the end point Url/Users/userid
    When  User clicks PUT request for userid
    Then  User should see the user details with id in response body with status code "201"
   
  @test02
  Scenario Outline: User Updates the details for invalid user_id
    Given User update user details in request body using end point Url/Users/userid
    When  User click PUT request for userid
    Then  User should see User id not Found in response body with status code "404"

   