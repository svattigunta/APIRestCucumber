
@GettingSkillId
Feature: Validate Get Skill Id Request from UserSkillMap

  @test01
  Scenario Outline: Validate UserSkillMap with authorization to get particular skill mapped with UserskillMap
    Given User enters the endpoint url/UserSkill/skillid for a particular skillid
    When  User click get method for particular skill id "<skillid>"
    Then  User should see skill with id,skillName all mapped with user with userid and details in response body with status code "200"

    Examples:
      |skillid|status  |
      | 2 | success |
      |02 | Fail    |
